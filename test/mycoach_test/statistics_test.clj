(ns mycoach-test.statistics-test
  (:require [clojure.test :refer :all]
            [mycoach-test.statistics :refer :all]
            [clojure.pprint :refer [pprint]]
            [clj-time.core :as t]
            [clojure.algo.generic.functor :refer [fmap]]
            [mycoach-test.sample-games :refer :all]))

(deftest team-stat-test
  (testing "team-stat shall return all values to 0 is empty games"
    (is (= {
            :home/win  0
            :home/draw 0
            :home/lost 0
            :away/win  0
            :away/draw 0
            :away/lost 0
            }
           (team-stats {:team_id "any id" :games_home [] :games_away []})
           (team-stats {:team_id "any id"})
           )))
  (testing "team-stats"
    (is (= stats-team1 (team-stats team1-with-game)))
    (is (= stats-team2 (team-stats team2-with-game)))
    (is (= stats-team3 (team-stats team3-with-game)))
    (is (= stats-team1-opp (team-stats team1-opp-with-game)))
    (is (= stats-team2-opp (team-stats team2-opp-with-game)))
    (is (= stats-team3-opp (team-stats team3-opp-with-game)))
    ))

