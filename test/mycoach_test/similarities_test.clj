(ns mycoach-test.similarities-test
  (:require [clojure.test :refer :all]
            [clojure.pprint :refer [pprint]]
            [clojure.algo.generic.functor :refer [fmap]]
            [mycoach-test.sample-games :refer :all]
            [mycoach-test.similarities :refer :all])
  )

(def critics
  {"Lisa Rose" {"Lady in the Water" 2.5 "Snakes on a Plane" 3.5
                "Just My Luck" 3.0 "Superman Returns" 3.5
                "You, Me and Dupree" 2.5 "The Night Listener" 3.0}
   "Gene Seymour" {"Lady in the Water" 3.0 "Snakes on a Plane" 3.5
                   "Just My Luck" 1.5  "Superman Returns" 5.0
                   "The Night Listener" 3.0 "You, Me and Dupree" 3.5}
   "Michael Phillips" {"Lady in the Water" 2.5 "Snakes on a Plane" 3.0
                       "Superman Returns" 3.5  "The Night Listener" 4.0}
   "Claudia Puig" {"Snakes on a Plane" 3.5 "Just My Luck" 3.0
                   "The Night Listener" 4.5 "Superman Returns" 4.0
                   "You, Me and Dupree" 2.5}
   "Mick LaSalle" {"Lady in the Water" 3.0 "Snakes on a Plane" 4.0
                   "Just My Luck" 2.0 "Superman Returns" 3.0
                   "The Night Listener" 3.0 "You, Me and Dupree" 2.0},
   "Jack Matthews" {"Lady in the Water" 3.0 "Snakes on a Plane" 4.0
                    "The Night Listener" 3.0 "Superman Returns" 5.0
                    "You, Me and Dupree" 3.5}
   "Toby" {"Snakes on a Plane" 4.5 "You, Me and Dupree" 1.0
           "Superman Returns" 4.0}})

(def lisa-rose-euclidean-sims
  '(
    ["Michael Phillips" 0.4444444444444444]
    ["Mick LaSalle" 0.3333333333333333]
    ["Claudia Puig" 0.2857142857142857]
    ["Toby" 0.2222222222222222]
    ["Jack Matthews" 0.21052631578947367]
    ["Gene Seymour" 0.14814814814814814]
    ))

(def toby-pearson-sims
  '(
    ["Lisa Rose" 0.9912407071619299]
    ["Mick LaSalle" 0.9244734516419049]
    ["Claudia Puig" 0.8934051474415647]
    ["Jack Matthews" 0.66284898035987]
    ["Gene Seymour" 0.38124642583151164]
    ["Michael Phillips" -1.0]
    ))

(deftest pearson-test
  (testing "pearson"
    (is (= 0.39605901719066977 (pearson (critics "Lisa Rose") (critics "Gene Seymour"))))
    ))

(deftest similarities-test
  (testing "sort-by-similarity with euclidean function"
    (is (= (into {} lisa-rose-euclidean-sims) (similarities euclidean critics "Lisa Rose")))
    (is (= (into {} toby-pearson-sims) (similarities pearson critics "Toby")))
    ))

(deftest top-matches-test
  (testing "top-matches"
    (is (= (take 3 lisa-rose-euclidean-sims)
           (top-matches 3 euclidean critics "Lisa Rose")))
    ))

(def toby-recos
  {"The Night Listener" 3.3477895267131017
   "Lady in the Water"  2.8325499182641614
   "Just My Luck"       2.530980703765565})

(deftest recommend-test
  (testing "recommend"
    (is (= toby-recos (recommend pearson critics "Toby")))
  ))

