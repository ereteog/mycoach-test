(ns mycoach-test.recommend-test
  (:require [clojure.test :refer :all]
            [mycoach-test.recommend :refer :all]
            [mycoach-test.sample-games :refer :all]
            [clojure.set :as s]
            [clojure.pprint :refer [pprint]]
  ))

(def sample-user-reco
  (file->user-recommender "resources/pref-sample.csv"
                          user-sim-pearson
                          0.1))
(deftest recommend-test
  (testing "recommend"
    (is (= #{12 13 14}
           (-> (recommend sample-user-reco 2 3)
               keys
               set)))
    ))

(deftest ids->long-ids-map-test
  (testing "ids->long-ids-map"
    (is (= {0 "0" 1 "1" 2 "2"}
           (ids->long-ids-map ["0" "1" "2"])))
  ))

(deftest prefs->long-prefs-test
  (testing "prefs->long-prefs"
    (is (= all-long-prefs
           (prefs->long-prefs (s/map-invert ids-map)
                              all-prefs)))
    ))

(deftest flatten-prefs-test
  (testing "flatten-prefs"
    (is (= flattened-all-prefs
           (flatten-prefs all-prefs)))
    (is (= [[0 1 1] [0 2 1.5] [1 0 5]]
           (flatten-prefs {0 {1 1
                              2 1.5}
                           1 {0 5}})))
    ))

