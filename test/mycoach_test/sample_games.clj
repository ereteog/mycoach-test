(ns mycoach-test.sample-games
  (:require [clj-time.core :as t]))

(def team1 {:club_id "2db2991b-4c95-11e5-a731-0242ac110002", :team_id "3aa8bb94-d752-508e-b651-03e3bc05788f", :zip "42400"})
(def team1-opp {:team_id "22d6c6ec-4cac-11e5-a731-0242ac110002"})
(def team2 {:club_id "39e2703b-4c95-11e5-a731-0242ac110002", :team_id "fdb4c45b-4caf-11e5-a731-0242ac110002", :zip "76800"})
(def team2-opp {:team_id "2484ec22-4afe-4226-aac8-8cf41913a906"})
(def team3 {:club_id "46527cec-4c95-11e5-a731-0242ac110002", :team_id "6f03ed7f-4caf-11e5-a731-0242ac110002", :zip "1871"})
(def team3-opp {:team_id "a83c94e4-0a10-4643-b7b9-b1e9ca3b59c2"})

(def all-teams
  [team1 team2 team3 team1-opp team2-opp team3-opp])

(def goals-games-4-0
  [{:goal_id "1de3c11f-ac6d-4f97-b0b4-bdf7dc759c63",
     :goal_minute "80",
     :goal_period "2",
     :goal_football_player_scorer_id
     "2e3179fe-4c52-11e5-a731-0242ac110002",
     :goal_football_game_id "2f49f08d-b996-414d-acf9-f99c0178e5e1",
     :goal_football_team_id "fdb4c45b-4caf-11e5-a731-0242ac110002",
     :goal_body_part "PIED_DROIT",
     :goal_area "SURFACE",
     :goal_type "CONTRE_ATTAQUE"}
    {:goal_id "3a85c52d-5ae8-49d1-a6af-5f022481f726",
     :goal_minute "87",
     :goal_period "2",
     :goal_football_player_scorer_id
     "2e3179fe-4c52-11e5-a731-0242ac110002",
     :goal_football_game_id "2f49f08d-b996-414d-acf9-f99c0178e5e1",
     :goal_football_team_id "fdb4c45b-4caf-11e5-a731-0242ac110002",
     :goal_body_part "PIED_DROIT",
     :goal_area "CENTRE",
     :goal_type "CONTRE_ATTAQUE"}
    {:goal_id "5b2df1ad-5fc4-46e7-a4dd-b829a5510dbc",
     :goal_minute "16",
     :goal_period "1",
     :goal_football_player_scorer_id
     "1a7b142d-4c57-11e5-a731-0242ac110002",
     :goal_football_game_id "2f49f08d-b996-414d-acf9-f99c0178e5e1",
     :goal_football_team_id "fdb4c45b-4caf-11e5-a731-0242ac110002",
     :goal_body_part "PIED_DROIT",
     :goal_area "SURFACE",
     :goal_type "ACTION_DE_JEU"}
    {:goal_id "94df5494-30e6-45a9-bc06-6f1c528b91a8",
     :goal_minute "15",
     :goal_period "1",
     :goal_football_player_scorer_id
     "1a7b142d-4c57-11e5-a731-0242ac110002",
     :goal_football_game_id "2f49f08d-b996-414d-acf9-f99c0178e5e1",
     :goal_football_team_id "fdb4c45b-4caf-11e5-a731-0242ac110002",
     :goal_body_part "TETE",
     :goal_area "6METRES",
     :goal_type "CORNER"}])

(def goals-game-0-2
  [{:goal_id "a32a4670-234d-42c7-8606-996767b40b53",
    :goal_minute "25",
    :goal_period "1",
    :goal_football_player_scorer_id
    "27190886-bb68-5c05-a94f-f052f7b58439",
    :goal_football_game_id "553dba86-f7d6-4fe5-8abc-32922f8f959d",
    :goal_football_team_id "3aa8bb94-d752-508e-b651-03e3bc05788f",
    :goal_body_part "AUTRE",
    :goal_area "NULL",
    :goal_type "ACTION_DE_JEU"}
   {:goal_id "d7671f56-7f42-4bda-909d-7daf4901916b",
    :goal_minute "54",
    :goal_period "2",
    :goal_football_player_scorer_id
    "9a4c5ffe-c6f0-5b00-905d-fe83eb86c457",
    :goal_football_game_id "553dba86-f7d6-4fe5-8abc-32922f8f959d",
    :goal_football_team_id "3aa8bb94-d752-508e-b651-03e3bc05788f",
    :goal_body_part "AUTRE",
    :goal_area "NULL",
    :goal_type "ACTION_DE_JEU"}])

(def game-0-4
  {:game_id "2f49f08d-b996-414d-acf9-f99c0178e5e1",
   :game_date (t/date-time 2016 1 17 13 0 0 0),
   :game_duration "90",
   :game_home_football_team_id "2484ec22-4afe-4226-aac8-8cf41913a906",
   :game_away_football_team_id "fdb4c45b-4caf-11e5-a731-0242ac110002",
   :goals goals-games-4-0
   })

(def game-4-0
  {:game_id "2f49f08d-b996-414d-acf9-f99c0178e5e1",
   :game_date (t/date-time 2016 1 17 13 0 0 0),
   :game_duration "90",
   :game_home_football_team_id "fdb4c45b-4caf-11e5-a731-0242ac110002",
   :game_away_football_team_id "2484ec22-4afe-4226-aac8-8cf41913a906",
   :goals goals-games-4-0
   })

(def game-0-0
  {:game_id "50d981a2-d4fe-486a-840a-4253b2a14931",
   :game_date (t/date-time 2015 7 20 17 0 0 0),
   :game_duration "90",
   :game_home_football_team_id "a83c94e4-0a10-4643-b7b9-b1e9ca3b59c2",
   :game_away_football_team_id "6f03ed7f-4caf-11e5-a731-0242ac110002",
   :goals []}
  )

(def game-0-2
  {:game_id "553dba86-f7d6-4fe5-8abc-32922f8f959d",
   :game_date (t/date-time 2015 11 1 8 0 0 0),
   :game_duration "90",
   :game_home_football_team_id "22d6c6ec-4cac-11e5-a731-0242ac110002",
   :game_away_football_team_id "3aa8bb94-d752-508e-b651-03e3bc05788f",
   :goals goals-game-0-2
   })

(def games
  [game-0-0 game-0-2 game-4-0 game-0-4])

(def team1-with-game
  (assoc team1
         :games_home []
         :games_away [game-0-2]))
(def team1-opp-with-game
  (assoc team1-opp
         :games_home [game-0-2]
         :games_away []))
(def team2-with-game
  (assoc team2
         :games_home [game-4-0]
         :games_away [game-0-4]))
(def team2-opp-with-game
  (assoc team2-opp
         :games_home [game-0-4]
         :games_away [game-4-0]))
(def team3-with-game
  (assoc team3
         :games_home []
         :games_away [game-0-0]))
(def team3-opp-with-game
  (assoc team3-opp
         :games_home [game-0-0]
         :games_away []))

(def all-teams-with-games
  [team1-with-game team2-with-game team3-with-game team1-opp-with-game team2-opp-with-game team3-opp-with-game])

(def stats-team1
  {
   :home/win  0
   :home/draw 0
   :home/lost 0
   :away/win  1
   :away/draw 0
   :away/lost 0
   })

(def stats-team1-opp
  {
   :home/win  0
   :home/draw 0
   :home/lost 1
   :away/win  0
   :away/draw 0
   :away/lost 0
   })

(def stats-team2
  {
   :home/win  1
   :home/draw 0
   :home/lost 0
   :away/win  1
   :away/draw 0
   :away/lost 0
   })

(def stats-team2-opp
  {
   :home/win  0
   :home/draw 0
   :home/lost 1
   :away/win  0
   :away/draw 0
   :away/lost 1
   })

(def stats-team3
  {
   :home/win  0
   :home/draw 0
   :home/lost 0
   :away/win  0
   :away/draw 1
   :away/lost 0
   })

(def stats-team3-opp
  {
   :home/win  0
   :home/draw 1
   :home/lost 0
   :away/win  0
   :away/draw 0
   :away/lost 0
   })

(def all-prefs
  {(:team_id team1)
   {(:team_id team1-opp) 5},
   (:team_id team2)
   {(:team_id team2-opp) 5},
   (:team_id team3)
   {(:team_id team3-opp) 3.5},
   (:team_id team1-opp)
   {(:team_id team1) 1},
   (:team_id team2-opp)
   {(:team_id team2) 1},
   (:team_id team3-opp)
   {(:team_id team3) 3}})

(def ids-map
  {
   0 (:team_id team1)
   1 (:team_id team2)
   2 (:team_id team3)
   3 (:team_id team1-opp)
   4 (:team_id team2-opp)
   5 (:team_id team3-opp)
   })

(def all-long-prefs
  {0 {3 5}
   1 {4 5}
   2 {5 3.5}
   3 {0 1}
   4 {1 1}
   5 {2 3}})

(def flattened-all-prefs
  [[(:team_id team1) (:team_id team1-opp) 5]
   [(:team_id team2) (:team_id team2-opp) 5]
   [(:team_id team3) (:team_id team3-opp) 3.5]
   [(:team_id team1-opp) (:team_id team1) 1]
   [(:team_id team2-opp) (:team_id team2) 1]
   [(:team_id team3-opp) (:team_id team3) 3]])
