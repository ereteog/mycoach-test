(ns mycoach-test.core-test
  (:require [clojure.test :refer :all]
            [mycoach-test.core :refer :all]
            [mycoach-test.parse :as parse]
            [mycoach-test.views :as views]
            [mycoach-test.statistics :as stats]
            [mycoach-test.similarities :as sim]
            [mycoach-test.recommend :as reco]
            [clojure.java.io :as io]
            [clojure.pprint :refer [pprint]]
            [clj-time.core :as t]
            [clj-time.format :as f]
            [clojure.algo.generic.functor :refer [fmap]])
  )

(with-open [teams_rdr (io/reader "resources/club-teams.csv")
            games_rdr (io/reader "resources/games.csv")
            goals_rdr (io/reader "resources/goals.csv")]
  (let [teams                 (parse/parse-teams teams_rdr)
        goals                 (parse/parse-goals goals_rdr)
        games                 (-> (parse/parse-games games_rdr)
                                  (views/games-with-goals goals))
        games-2015-2016       (->> (views/filter-by-season games 2015 :game_date)
                                   (sort-by :game_date t/before?))
        teams-games-2015-2016 (views/teams-with-games teams games-2015-2016)
        asc-nb-games          (sort-by #(+ (count (:games_home %)))
                                       <
                                       teams-games-2015-2016)
        mycoach-teams-prefs   (views/teams->prefs teams-games-2015-2016)
        all-teams-2015-2016   (views/games->teams games-2015-2016)
        asc-nb-games-all      (sort-by #(+ (count (:games_home %)))
                                       <
                                       all-teams-2015-2016)
        all-teams-prefs       (views/teams->prefs all-teams-2015-2016)
        ids-map               (reco/ids->long-ids-map (map :team_id all-teams-2015-2016))
        inv-ids-map           (clojure.set/map-invert ids-map)
        recommender           (reco/file->user-recommender "test.csv" reco/user-sim-pearson 0.1)
        ]
    (println "nb teams: " (count teams))
    (println "nb games: " (count games))
    (println "nb goals: " (count goals))
    (println "count games-2015-2016")
    (println (count games-2015-2016))
    (println "first game 2015-2016")
    (pprint (first games-2015-2016))
    (println "last game 2015-2016")
    (pprint (last games-2015-2016))
    (println "min games")
    (pprint (first asc-nb-games))
    (println "stats max games")
    (pprint (stats/team-stats (last asc-nb-games)))
    (println "stats max all games" )
    (pprint (stats/team-stats (last asc-nb-games-all)))
    (pprint "rand stats")
    (pprint (stats/team-stats (rand-nth teams-games-2015-2016)))
    (println "recommend" (:team_id (last asc-nb-games-all)))
    (pprint (get all-teams-prefs (:team_id (last asc-nb-games-all))))
    (pprint (->> (sim/similarities sim/euclidean mycoach-teams-prefs (:team_id (last asc-nb-games)))
                 (filter #(< 0 (second %)))))
    (pprint (reco/recommend recommender (get inv-ids-map (:team_id (last asc-nb-games))) 3))
    ))

