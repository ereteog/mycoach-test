(ns mycoach-test.views-test
  (:require [clojure.test :refer :all]
            [mycoach-test.views :refer :all]
            [clojure.pprint :refer [pprint]]
            [clj-time.core :as t]
            [clojure.algo.generic.functor :refer [fmap]]
            [mycoach-test.sample-games :refer :all])
  )

(deftest score-test
  (testing "score draw / away win / home win"
    (is (= {"a83c94e4-0a10-4643-b7b9-b1e9ca3b59c2" 0
            "6f03ed7f-4caf-11e5-a731-0242ac110002" 0}
           (score game-0-0)))
    (is (= {"22d6c6ec-4cac-11e5-a731-0242ac110002" 0
            "3aa8bb94-d752-508e-b651-03e3bc05788f" 2}
           (score game-0-2)))
    (is (= {"fdb4c45b-4caf-11e5-a731-0242ac110002" 4
            "2484ec22-4afe-4226-aac8-8cf41913a906" 0}
           (score game-4-0)
           (score game-4-0)))
    ))

(deftest winner-test
  (testing "winner"
    (is (= "3aa8bb94-d752-508e-b651-03e3bc05788f"
           (winner game-0-2)))
    (is (= nil (winner game-0-0)))
    (is (= "fdb4c45b-4caf-11e5-a731-0242ac110002" (winner game-4-0)))
    (is (= "fdb4c45b-4caf-11e5-a731-0242ac110002" (winner game-0-4)))
  ))

(deftest team-with-games-test
  (testing "team-with-games"
    (= all-teams-with-games (teams-with-games all-teams games))
    ))

(deftest prefs-test
  (let [team2_id (:team_id team2)
        game-0-0-team2 (assoc game-0-0 :game_away_football_team_id team2_id)]
    (testing "games->pref"
      (is (= 3.5    (games->pref team2_id [game-0-0-team2])))
      (is (= 4.25   (games->pref team2_id [game-0-0-team2 game-4-0])))
      (is (= 4.5    (games->pref team2_id [game-0-0-team2 game-4-0 game-0-4])))
      )
    (testing "teams->prefs"
      (is (= all-prefs (teams->prefs all-teams-with-games)))
      )
    ))


