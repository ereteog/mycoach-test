# mycoach-test

## consigne

"You will find in attachment a set of data in files that should give you the opportunity to demonstrate an insightful analysis of them. Feel free to use all the options at your disposal to highlight your analytical capabilities and how you would make those data live."

Les données sont dans "resources":

* club-teams.csv
* games.csv
* goals.csv
* players.csv

## objectif initial

Mon objectif initial était d'estimer le résultat d'un match à venir et suggérer des équipes pour l'organisation de matchs. Mon hypothèse est qu'en exploitant les résultats des différentes équipes, nous pouvons définir les "préférences" de chaque équipe et donc ramener ce problème à un problème de recommandation classique.

## résultats obtenus
Ce projet permet:

* D'obtenir différente vue sur les données de MyCoach, afin de connecter les données (match/buts, équipes/matchs, clubs/équipes) et les filtrer par saison.
* D'obtenir les statistiques de résultats d'une équipe à domicile et à l'extérieur.
* De calculer les préférences d'une équipes par rapport à d'autres équipes, en calculant des notes entre équipe à partir des résultats.
* D'effectuer des recommandations d'équipes à partir de ces recommandations.
* Analyse du graphe formé par les matchs.

### calcul des préférences d'une équipe
Sur le modèle des notations de produits en ligne chaque équipe obtient un ensemble de notes de 1 à 5 pour les équipes qu'elle a affronté:

* 1 si défaite avec au moins 2 buts d'écart
* 2 si défaite avec 1 but d'écart
* 3 si match nul
* 4 si victoire avec 1 but d'écart
* 5 si victoire avec au moins 2 buts d'écart
* bonus de match à l'extérieur: 0.5

Une moyenne est effectutée pour les équipes jouées plusieurs fois.

Ce modèle est assez simple et pourrait être enrichi avec d'autres éléments, tels que les moments où les buts ont été marqués (maitrise du match ou non?), les types de buts (penalty moins valorisé?), etc.

ex: préférences de l'équipe "fdb4c45b-4caf-11e5-a731-0242ac110002"

	{"4db49a6d-4eed-4161-94f8-3605560fba79" 5,
	 "138dec4a-843a-4f74-8cd6-343ca2bc5f87" 5,
	 "78d8f6ca-641a-4def-aee9-afeabf7d5bad" 3,
	 "a01df6e7-7167-4b4c-9f61-6391dd107470" 3.5,
	 "1c323261-12e8-4f22-9cbf-d5be465b1fdb" 5,
	 "73473ca1-4f8f-4cb5-976f-5eb02ef3b37e" 3.5,
	 "a9fa3b6f-4174-4b57-86a3-66be64965afd" 5,
	 "911a30a2-598c-4621-be02-fb67fa76f014" 3,
	 "5b283c99-cd7a-4593-a51c-0257a2d24736" 4.5,
	 "025e5053-3f24-4518-bcd8-c9a3f8b0797d" 5,
	 "2484ec22-4afe-4226-aac8-8cf41913a906" 5,
	 "c6f84a6f-bd32-41f9-91da-1c70eb82d1de" 3.5,
	 "cb97f80e-090e-46ba-902f-75aa19ae5b26" 5,
	 "51b64c63-c10b-41c9-bb7a-9f87b452c3f9" 3,
	 "5224443c-30d6-487a-892c-01153257d479" 3.5,
	 "3f881cb6-bd47-4942-b69b-56f2e540098c" 3,
	 "1e2fd87d-2fd3-4d7a-8c73-1e40c9800d3d" 5,
	 "c9431475-ecb6-4fca-830a-ca71a3763de3" 5,
	 "e76d1ce7-3a78-402c-839d-07fdbaf0378e" 4,
	 "9c69f7ef-b418-49b7-9857-71d3c47618a8" 5,
	 "fdf0ceea-9b22-4ec0-842b-9cc81510e33c" 3,
	 "dc25fa34-2965-4dd6-ba4e-f6df4e8ba67e" 4.5,
	 "7309d67b-f0d5-4517-85a2-ab0e5ee17646" 5,
	 "e9897a9f-ce29-42d8-a1e6-a13169fcd498" 5
	 ...}

### recommandation
J'ai tout d'abord repris une implémentation en clojure du chapitre sur la recommandation, collaborative filtering, du livre d'Oreilly "programming collective intelligence". Obtenant peu de recommandation avec cette approche user based classique, j'ai analysé plus en détail la structure des données à partir du graphe des matchs (voire analyse du graphe) qui a révélé en particulier un manque de connectivité de ce graphe ce qui réduit l'efficacité des approches de recommandation de type collaborative filtering user based sur lequel je me suis orienté. 

Pour obtenir la meilleur solution collaborative filtering je me suis réorienté sur la librairie [Mahout](http://mahout.apache.org/ "Mahout"). Cette librairie implémente plusieurs fonctions de similarité (euclidean, pearson, cityblock, cosine, tanimoto, likelihood), de voisinnage (nearestN, threshold) et de recommandation (user based et item based). De plus Mahout propose un framework d'évaluation (cross validation sur le dataset de training) que j'ai exploité pour trouver la meilleure combinaison.

Je me suis focalisé sur l'analyse de la saison 2015/2016 qui contient le plus de matchs (matchs du 1 Juillet 2015 au 30 Juin 2016). Selon la cross validation de Mahout, les meilleurs résultats pour user based est avec la fonction de voisinnage threshold et la fonction de similarité euclidean. Pour item based la meilleure fonction de similarité est cityblock. Afin d'augmenter le nombre de recommandations, j'ai ajouté la possibilité de mixer les fonctions de similarité (sans apport sur ces données). Le fichier resources/recos.txt montre des recommandations pour les 100 équipes ayant le plus de matchs.

## analyse du graphe

Le graphe des matchs est généré ainsi: chaque noeud représente une équipe et une arrête représente un match.
Certaines équipes s'étant rencontrées plus d'une fois nous obtenons un graphe non orienté de 9722 noeud et 9101 arrêtes.
Si on ne conserve que les équipes qui ont au moins 2 arrêtes (c'est à dire 2 adversaires), le graphe ne contient plus que 712 noeud et 362 arrêtes, ce qui laisse un petit noyau faiblement connecté (il y a plus de relations vers l'exterieur de ce noyau, 9101 - 362, qu'à l'intérieur, 362).

La figure suivante montre une visualisation de ce noyau fortement déconnecté (version pdf dans resources)

![visualisation noyau](resources/visualisation-noyau.png)

La figure suivante montre la distribution globale des degrés avec majoritairement des équipes avec une seule relation

![degree distribution](resources/degree-distribution.png)

Ce graphe est fortement modulaire avec 646 communautés détectées, la figure suivante montre la distribution des tailles de ces communautés

![communauty size distribution](resources/communities-size-distribution.png)

Le distance moyenne entre deux équipes dans les composantes connexes de ce graphe est 7,65 (max: 25).

Les différentes resources et métriques ont été calculée avec un wrapper de gephi toolkit que je développe actuellement [clj-gephi](http://github.com/ereteog/clj-gephi  "clj-gephi")

anomalie? Dans les équipes étant associée à un club, il n'y a quasiment pas de match à l'extérieur. Mes tests unitaires me montre que ma fonction de regroupement permettant d'associer à une équipe l'ensemble de ses match à domicile et exterieur est valide ainsi que la fonction de calcul des statistiques.

## exploitation des résultats / perspectives
Les données manquant de connectivité, il serait intéressant de les connecter à des données externes donnant les résultats pour les matchs afin de compléter le graphe des matchs. Avec la connectivité suffisante nous pourrions imaginer deux scénarios: 
* utiliser l'item based recommender pour suggérer des équipes similaires à un futur adversaire en vue de l'organisation d'un match.
* utiliser le user based recommender pour suggérer des adversaires qu'une équipe pourrait gagner
* estimer le résutlat d'un match en exploitant les fonctions de similarités (pas forcément la meilleure approche pour la prédiction mais ce serait un usage de cet outil)

## organisation du projet

mycoach-test.parse: fonctions permettant de transformer les sources CSV en Clojure edn

mycoach-test.views: fonctions permettant de filtrer et grouper les différentes structures. En particulier, on peut filtrer les matchs par saison, associer les buts au matchs, grouper les matchs par équipe, obtenir les scores des matchs, et tranformer les matchs d'une équipe en ensemble de préférences.

mycoach-test.statistics: contient une seule fonction principale, qui prend en paramètre une équipe et des matchs à domicile et à l'extérieur et qui renvoie ses résultats (victoire, défaite et nul) à domicile et à l'extérieur.

mycoach.similarities: implémentation du chapitre sur la recommandation de "programming collective intelligence" de O'Reilly

mycoach.recommend: wrapper du standalone recommender de Mahoot + des fonctions de formattage des préférences (mahout a besoin d'ids de type long).

Ces différentes fonctions sont testées dans le namespace du même nom avec le suffixe "-test"

## usage

installer leiningen http://leiningen.org/

lein run
