(ns mycoach-test.parse
  (:require [clojure.data.csv :as csv]
            [clojure.java.io :as io]
            [clojure.string :refer [lower-case]]
            [clj-time.core :as t]
            [clj-time.format :as f]
            )
  )

(defn parse-date
  "parse mycoach football date"
  [date-str]
  (when date-str
    (f/parse (f/formatter "yyyy-MM-dd HH:mm:ss")
             date-str)))

(defn header->keys
  "transform header list into edn keys prefixed with given prefix"
  [header prefix]
  (map #(->> (str prefix %)
             lower-case
             keyword)
       header))

(defn parse-csv
  "generic mycoach csv parser"
  [prefix rdr]
  (let [[header & csv-lines] (csv/read-csv rdr)
        resource-keys (header->keys header prefix)
        resources (map #(zipmap resource-keys %) csv-lines)]
    resources))

(defn parse-games
  "parse with game key prefix, and format game date"
  [rdr]
  (->> (parse-csv "game_" rdr)
       (map #(update % :game_date parse-date))))

(def parse-goals (partial parse-csv "goal_"))

(def parse-teams (partial parse-csv ""))

