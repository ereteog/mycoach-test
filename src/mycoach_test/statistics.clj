(ns mycoach-test.statistics
  (:require [mycoach-test.views :as views]
            [clojure.pprint :refer [pprint]]
            [clojure.algo.generic.functor :refer [fmap]]
            ))

(defn stat-append-game
  [team-id ns stats game]
  (condp (views/winner game) =
    team-id (update game (keyword ns "win") inc)
    nil     (update game (keyword ns "draw") inc)
    (update game (keyword ns "lost") inc)))

(defn team-stats
  [{team-id    :team_id
    games-home :games_home
    games-away :games_away}]
  (let [winners-home (group-by views/winner games-home)
        winners-away (group-by views/winner games-away)
        winners-home-count (fmap count winners-home)
        winners-away-count (fmap count winners-away)
        res-stats    {
                      :home/win  (get winners-home-count team-id 0)
                      :home/draw (get winners-home-count nil 0)
                      :home/lost (->> (dissoc winners-home-count nil team-id)
                                      vals
                                      (apply +))
                      :away/win  (get winners-away-count team-id 0)
                      :away/draw (get winners-away-count nil 0)
                      :away/lost (->> (dissoc winners-away-count nil team-id)
                                      vals
                                      (apply +))
                      }]
    res-stats
    ))

