(ns mycoach-test.core
  (:require [mycoach-test.parse :as parse]
            [mycoach-test.views :as views]
            [mycoach-test.statistics :as stats]
            [mycoach-test.similarities :as sim]
            [mycoach-test.recommend :as reco]
            [clojure.java.io :as io]
            [clojure.pprint :refer [pprint]]
            [clj-time.core :as t]
            [clj-time.format :as f]
            [clojure.algo.generic.functor :refer [fmap]])
  )

(defn -main [& args]
  (with-open [teams_rdr (io/reader "resources/club-teams.csv")
              games_rdr (io/reader "resources/games.csv")
              goals_rdr (io/reader "resources/goals.csv")]
    (let [teams                 (parse/parse-teams teams_rdr)
          goals                 (parse/parse-goals goals_rdr)
          games                 (-> (parse/parse-games games_rdr)
                                    (views/games-with-goals goals))
          games-2015-2016       (->> (views/filter-by-season games 2015 :game_date)
                                     (sort-by :game_date t/before?))
          teams-games-2015-2016 (views/teams-with-games teams games-2015-2016)
          asc-nb-games          (sort-by #(+ (count (:games_home %)))
                                         >
                                         teams-games-2015-2016)
          mycoach-teams-prefs   (views/teams->prefs teams-games-2015-2016)
          all-teams-2015-2016   (views/games->teams games-2015-2016)
          asc-nb-games-all      (sort-by #(+ (count (:games_home %)))
                                         <
                                         all-teams-2015-2016)
          all-teams-prefs       (views/teams->prefs all-teams-2015-2016)
          ids-map               (reco/ids->long-ids-map (map :team_id all-teams-2015-2016))
          inv-ids-map           (clojure.set/map-invert ids-map)
          ]
      (println "nb teams: " (count teams))
      (println "nb games: " (count games))
      (println "nb goals: " (count goals))
      (println "count games-2015-2016")
      (println (count games-2015-2016))
      (println "first game 2015-2016")
      (pprint (first games-2015-2016))
      (println "last game 2015-2016")
      (pprint (last games-2015-2016))
      (println "stats 10 max games")
      (println "prefs of " (:team_id (last asc-nb-games-all)))
      (pprint (get all-teams-prefs (:team_id (last asc-nb-games-all))))
      (println "custom recommendation for " (:team_id (last asc-nb-games-all))
               (count
                (->> (sim/similarities sim/euclidean mycoach-teams-prefs (:team_id (last asc-nb-games)))
                     (filter #(< -1 (second %))))))

      (println "evaluating recommenders...")
      (reco/evaluate-all "resources/teams-prefs-mahout.csv")
      
      (println "recommending teams for 100 teams with most games")
      (let [user-based-reco-euclidean (reco/file->user-based-threshold-recommender "resources/teams-prefs-mahout.csv"
                                                                                   reco/user-sim-euclidean
                                                                                   0.1)
            user-based-reco-cityblock (reco/file->user-based-threshold-recommender "resources/teams-prefs-mahout.csv"
                                                                                   reco/user-sim-cityblock
                                                                                   0.1)
            user-based-reco-cosine (reco/file->user-based-threshold-recommender "resources/teams-prefs-mahout.csv"
                                                                                reco/user-sim-cosine
                                                                                0.1)
            user-based-reco-tanimoto (reco/file->user-based-threshold-recommender "resources/teams-prefs-mahout.csv"
                                                                                  reco/user-sim-tanimoto
                                                                                  0.1)
            user-based-reco-loglikelihood (reco/file->user-based-threshold-recommender "resources/teams-prefs-mahout.csv"
                                                                                       reco/user-sim-loglikelihood
                                                                                       0.1)
            user-based-recommenders [user-based-reco-euclidean
                                     user-based-reco-cityblock
                                     user-based-reco-cosine
                                     user-based-reco-tanimoto
                                     user-based-reco-loglikelihood]
            item-based-cityblock (reco/file->item-based-recommender "resources/teams-prefs-mahout.csv"
                                                          reco/user-sim-cityblock)
            item-based-cosine (reco/file->item-based-recommender "resources/teams-prefs-mahout.csv"
                                                          reco/user-sim-cosine)
            item-based-tanimoto (reco/file->item-based-recommender "resources/teams-prefs-mahout.csv"
                                                          reco/user-sim-tanimoto)
            item-based-loglikelihood (reco/file->item-based-recommender "resources/teams-prefs-mahout.csv"
                                                                        reco/user-sim-loglikelihood)
            item-based-euclidean (reco/file->item-based-recommender "resources/teams-prefs-mahout.csv"
                                                                    reco/user-sim-euclidean)
            item-based-recommenders [item-based-euclidean
                                     item-based-cityblock
                                     item-based-cosine
                                     item-based-tanimoto
                                     item-based-loglikelihood]]
        (doseq [t (take 100 asc-nb-games)]
          (println (:team_id t))
          (pprint (stats/team-stats t))
          (let [long-id (get inv-ids-map (:team_id t))]
            (println "user based threshold euclidean"
                     (->> (reco/recommend user-based-reco-euclidean long-id 3)
                          (map (fn [[k v]] [(get ids-map k) v]))))
            (println "user based threshold cityblock"
                     (->> (reco/recommend user-based-reco-cityblock long-id 3)
                          (map (fn [[k v]] [(get ids-map k) v]))))
            (println "user based threshold cosine"
                     (->> (reco/recommend user-based-reco-cosine long-id 3)
                          (map (fn [[k v]] [(get ids-map k) v]))))
            (println "user based threshold tanimoto"
                     (->> (reco/recommend user-based-reco-cosine long-id 3)
                          (map (fn [[k v]] [(get ids-map k) v]))))
            (println "user based threshold loglikelihood"
                     (->> (reco/recommend user-based-reco-cosine long-id 3)
                          (map (fn [[k v]] [(get ids-map k) v]))))
            (println "mixed user based"
                     (->> (reco/mixed-recommend user-based-recommenders long-id 3)
                          (map (fn [[k v]] [(get ids-map k) v]))))
            (println "item based threshold euclidean"
                     (->> (reco/recommend item-based-euclidean long-id 3)
                          (map (fn [[k v]] [(get ids-map k) v]))))
            (println "item based threshold cityblock"
                     (->> (reco/recommend item-based-cityblock long-id 3)
                          (map (fn [[k v]] [(get ids-map k) v]))))
            (println "item based threshold cosine"
                     (->> (reco/recommend item-based-cosine long-id 3)
                          (map (fn [[k v]] [(get ids-map k) v]))))
            (println "item based threshold tanimoto"
                     (->> (reco/recommend item-based-cosine long-id 3)
                          (map (fn [[k v]] [(get ids-map k) v]))))
            (println "item based threshold loglikelihood"
                     (->> (reco/recommend item-based-cosine long-id 3)
                          (map (fn [[k v]] [(get ids-map k) v]))))
            (println "mixed item based: "
                     (->> (reco/mixed-recommend item-based-recommenders long-id 3)
                          (map (fn [[k v]] [(get ids-map k) v]))))
                )))
      )
    ))
