(ns mycoach-test.views
  (:require [clj-time.core :as t]
            [clojure.algo.generic.functor :refer [fmap]]
            [clojure.pprint :refer [pprint]]
            ))


;; -------- filterers -------
(defn filter-by-season
  "filter games by seasons according to given start year (2015 for 2015-2016)"
  [games year date-key]
  (let [start_date (t/date-time year 7 1)
        end_date   (t/date-time (inc year) 6 30)
        season-interval (t/interval start_date end_date)]
    (filter #(t/within? season-interval
                        (get % date-key))
            games)
    ))

(defn games-with-goals
  "associate goals to corresponding games"
  [games goals]
  (let [goals-by-game (group-by :goal_football_game_id goals)]
    (map #(->> (get goals-by-game (:game_id %) [])
               (assoc % :goals))
         games)))


(defn games-by-team_ids
  "group games by team_ids with away and home games' lists
  games -> {\"team_id\"}"
  [games]
  (let [by-home-team (fmap #(hash-map :games_home %)
                           (group-by :game_home_football_team_id games))
        by-away-team (fmap #(hash-map :games_away %)
                           (group-by :game_away_football_team_id games))]
    (merge-with into by-away-team by-home-team)))

(defn games->teams
  "return  a list of teams with away and home game from a list of game"
  [games]
  (map (fn [[team_id team_games]]
         (assoc team_games :team_id team_id))
       (games-by-team_ids games)))

(defn teams-with-games
  "return teams with its away and home games"
  [teams games]
  (let [g-by-team_ids (games-by-team_ids games)
        t-by-team_ids (group-by :team_id teams)
        merged        (merge-with into g-by-team_ids t-by-team_ids)]
    (->> (keys t-by-team_ids)
         (select-keys merged)
         vals)))

;;-------- scores --------
(defn- score-append-goal
  [score {team :goal_football_team_id}]
  (update score team inc))

(defn score
  "return the score of a game with goals"
  [{home-team :game_home_football_team_id
    away-team :game_away_football_team_id
    goals     :goals}]
  (reduce score-append-goal
          {home-team 0 away-team 0}
          goals))

(defn winner
  "return the winner of a game with goals"
  [game-with-goals]
  (let [[[team1 s1] [team2 s2]] (sort-by val (score game-with-goals))]
    (when (> s2 s1) team2)))

(defn- play-away?
  [team_id game]
  (= team_id (:game_away_football_team_id game)))

(defn game->pref
  "return preference from game"
  [team_id game-w-goals]
  (let [game-score (score game-w-goals)
        goal-diff  (- (get game-score team_id)
                      (-> (dissoc game-score team_id)
                          first
                          val))
        bonus-away (if (play-away? team_id game-w-goals) 0.5 0)
        ]
    (-> (+ 3 goal-diff bonus-away)
        (max 1)
        (min 5))
    ))

(defn games->pref
  "Computes the average game pref of a team for given list of games"
  [team_id games-w-goals]
  (let [sum-prefs (reduce #(+ %1 (game->pref team_id %2))
                          0
                          games-w-goals)]
    (/ sum-prefs (count games-w-goals))))

(defn- prefs-append-games
  [team_id prefs [opp-team_id games]]
  (assoc prefs opp-team_id (games->pref team_id games)))

(defn- append-team->prefs
  [prefs {team_id :team_id
          games-home :games_home
          games-away :games_away}]
  (let [home-by-opp  (group-by :game_away_football_team_id games-home)
        away-by-opp  (group-by :game_home_football_team_id games-away)
        games-by-opp (merge-with concat home-by-opp away-by-opp)]
    (assoc prefs
           team_id
           (reduce (partial prefs-append-games team_id)
                   {}
                   games-by-opp))
    ))

(defn teams->prefs
  "return map of preferences from teams"
  [teams-w-games]
  (reduce append-team->prefs {} teams-w-games))

