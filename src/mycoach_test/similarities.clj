(ns mycoach-test.similarities)


;; pearson http://nakkaya.com/2009/11/13/pearson-correlation-score/ 
(defn pearson [x y]
  (let [shrd (filter x (keys y))] 
    (if (= 0 (count shrd))
      0
      (let [sum1  (reduce (fn[s mv] (+ s (x mv))) 0 shrd)
            sum2  (reduce (fn[s mv] (+ s (y mv))) 0 shrd)
            sum1sq  (reduce (fn[s mv] (+ s (Math/pow (x mv) 2))) 0 shrd)
            sum2sq  (reduce (fn[s mv] (+ s (Math/pow (y mv) 2))) 0 shrd)
            psum (reduce (fn[s mv] (+ s (* (x mv) (y mv)))) 0 shrd)
            num (- psum (/ (* sum1 sum2) (count shrd)))
            den (Math/sqrt (* 
                            (- sum1sq (/ (Math/pow sum1 2) (count shrd)))
                            (- sum2sq (/ (Math/pow sum2 2) (count shrd)))))]
        (if (= den 0)
          0
          (double (/ num den))) ))))

;; euclidean http://nakkaya.com/2009/11/11/euclidean-distance-score/
(defn euclidean [person1 person2]
  (let [shared-items (filter person1 (keys person2))
        score (reduce (fn[scr mv]
                        (let [score1 (person1 mv)
                              score2 (person2 mv)]
                          (+ scr (Math/pow (- score1 score2) 2))))
                      0 shared-items)]
    (if (= (count shared-items) 0)
      0
      (/ 1 (+ 1 score)))))

;; recommend http://nakkaya.com/2009/11/21/making-recommendations/

(defn similarities
  ([sim-fun prefs team_id]
   (let [team_prefs (get prefs team_id {})]
     (->> (reduce (fn [h [name other_prefs]]
                    (->> (sim-fun other_prefs team_prefs)
                         (assoc h name)))
                  {}
                  (dissoc prefs team_id)))))
  ([sim-fun prefs team_id threshold]
   (->> (similarities sim-fun prefs team_id)
        (filter #(< threshold (second %)))
        (into {}))))

(defn top-matches
  [nb sim-fun prefs team_id]
  (->> (similarities sim-fun prefs team_id)
       (sort-by second >)
       (take nb))
  )

(defn weight-prefs [prefs similarities person]
  (reduce
   (fn [h v]
     (let [other (first v) score (second v)
           diff (filter #(not (contains? (prefs person) (key %))) (prefs other))
           weighted-pref (apply hash-map
                                (interleave (keys diff)
                                            (map #(* % score) (vals diff))))]
       (assoc h other weighted-pref))) {} similarities))

(defn sum-scrs [prefs]
  (reduce (fn [h m] (merge-with #(+ %1 %2) h m)) {} (vals prefs)))


(defn sum-sims [weighted-pref scores sim-users]
  (reduce (fn [h m]
            (let [movie (first m)
                  rated-users (reduce
                               (fn [h m] (if (contains? (val m) movie)
                                           (conj h (key m)) h))
                               [] weighted-pref)
                  similarities (apply + (map #(sim-users %) rated-users))]
              (assoc h movie similarities) ) ) {} scores))

(defn recommend [sim-fun prefs person]
  (let [similar-users (similarities sim-fun prefs person)
        weighted-prefs (weight-prefs prefs similar-users  person)
        scores (sum-scrs weighted-prefs)
        sims (sum-sims weighted-prefs scores similar-users)]
    (zipmap (keys scores)
            (map #(/ (second %) (sims (first %))) scores))))
