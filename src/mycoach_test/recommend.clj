(ns mycoach-test.recommend
  (:require [clojure.java.io :as io]
            [clojure.data.csv :as csv]
            [clojure.set :refer [map-invert]])
  (:import [org.apache.mahout.cf.taste.common TasteException])
  (:import [org.apache.mahout.cf.taste.impl.common LongPrimitiveIterator])
  (:import [org.apache.mahout.cf.taste.model DataModel])
  (:import [org.apache.mahout.cf.taste.impl.model.file FileDataModel])
  (:import [org.apache.mahout.cf.taste.impl.neighborhood ThresholdUserNeighborhood])
  (:import [org.apache.mahout.cf.taste.impl.neighborhood NearestNUserNeighborhood])
  (:import [org.apache.mahout.cf.taste.impl.similarity CityBlockSimilarity])
  (:import [org.apache.mahout.cf.taste.impl.similarity EuclideanDistanceSimilarity])
  (:import [org.apache.mahout.cf.taste.impl.similarity LogLikelihoodSimilarity])
  (:import [org.apache.mahout.cf.taste.impl.similarity PearsonCorrelationSimilarity])
  (:import [org.apache.mahout.cf.taste.impl.similarity TanimotoCoefficientSimilarity])
  (:import [org.apache.mahout.cf.taste.impl.similarity UncenteredCosineSimilarity])
  (:import [org.apache.mahout.cf.taste.neighborhood UserNeighborhood])
  (:import [org.apache.mahout.cf.taste.recommender RecommendedItem])
  (:import [org.apache.mahout.cf.taste.recommender UserBasedRecommender])
  (:import [org.apache.mahout.cf.taste.similarity UserSimilarity])
  (:import [org.apache.mahout.cf.taste.impl.recommender GenericUserBasedRecommender])
  (:import [org.apache.mahout.cf.taste.impl.recommender GenericItemBasedRecommender])
  (:import [org.apache.mahout.cf.taste.impl.similarity GenericItemSimilarity$ItemItemSimilarity])
  (:import [org.apache.mahout.cf.taste.eval RecommenderEvaluator])
  (:import [org.apache.mahout.cf.taste.impl.eval AverageAbsoluteDifferenceRecommenderEvaluator])
  (:import [org.apache.mahout.cf.taste.eval RecommenderBuilder])
  (:require [clojure.algo.generic.functor :refer [fmap]])
  )
;; https://mahout.apache.org/users/recommender/recommender-documentation.html

(defn file-data-model
  [filepath]
  (FileDataModel. (io/file filepath))
  )

(defn user-sim-cityblock
  [data-model]
  (CityBlockSimilarity. data-model))

(defn user-sim-euclidean
  [data-model]
  (EuclideanDistanceSimilarity. data-model))

(defn user-sim-loglikelihood
  [data-model]
  (LogLikelihoodSimilarity. data-model))

(defn user-sim-tanimoto
  [data-model]
  (TanimotoCoefficientSimilarity. data-model))

(defn user-sim-cosine
  [data-model]
  (UncenteredCosineSimilarity. data-model))

(defn user-sim-pearson
  [data-model]
  (PearsonCorrelationSimilarity. data-model))

(defn user-neighborhood-nearestN
  [nb user-similarity data-model]
  (NearestNUserNeighborhood. nb, user-similarity, data-model))

(defn user-neighborhood-threshold
  [threshold user-similarity data-model]
  (ThresholdUserNeighborhood. threshold, user-similarity, data-model))

(defn generic-item-based-recommender
  [model user-sim]
  (GenericItemBasedRecommender. model user-sim))

(defn generic-user-based-recommender
  [model neighborhood user-sim]
  (GenericUserBasedRecommender. model neighborhood user-sim))

(defn file->user-based-threshold-recommender
  "return user based recommender with threshold neighborhood from file data model"
  [filepath sim-fun threshold]
  (let [data-model   (file-data-model filepath)
        user-sim     (sim-fun data-model)
        neighborhood (user-neighborhood-threshold threshold user-sim data-model)]
    (generic-user-based-recommender data-model neighborhood user-sim)
    ))

(defn file->user-based-nearestN-recommender
  "return user based recommender with nearestN neighborhood from file data model"
  [filepath sim-fun n]
  (let [data-model   (file-data-model filepath)
        user-sim     (sim-fun data-model)
        neighborhood (user-neighborhood-nearestN n user-sim data-model)]
    (generic-user-based-recommender data-model neighborhood user-sim)
    ))

(defn file->item-based-recommender
  "return item based recommender from file data model"
  [filepath sim-fun]
  (let [data-model   (file-data-model filepath)
        user-sim     (sim-fun data-model)]
    (generic-item-based-recommender data-model user-sim)
    ))

(defn- recommend-item->hashmap
  [item]
  {(.getItemID item) (.getValue item)}
  )

(defn recommend
  "makes recommendation"
  [recommender id nb]
  (->> (.recommend recommender id nb)
       (map recommend-item->hashmap)
       (reduce into {})))

(defn mixed-recommend
  "mixes recommendations from given list of recommenders"
  [recommenders id nb]
  (->> (map #(recommend % id nb) recommenders)
       (reduce (partial merge-with +) {})
       (sort-by val >)
       (take nb)
       )
  )

(defn- user-based-recommend-threshold-builder
  [sim-fun threshold]
  (reify RecommenderBuilder
    (buildRecommender [this data-model]
      (let [user-sim     (sim-fun data-model)
            neighborhood (user-neighborhood-threshold threshold user-sim data-model)]
        (generic-user-based-recommender data-model neighborhood user-sim))
      )))

(defn- user-based-recommend-nearest-builder
  [sim-fun n]
  (reify RecommenderBuilder
    (buildRecommender [this data-model]
      (let [user-sim     (sim-fun data-model)
            neighborhood (user-neighborhood-nearestN n user-sim data-model)]
        (generic-user-based-recommender data-model neighborhood user-sim))
      )))

(defn- item-based-recommend-builder
  [sim-fun]
  (reify RecommenderBuilder
    (buildRecommender [this data-model]
      (->> (sim-fun data-model)
           (generic-item-based-recommender data-model))
      )))

(defn evaluate-item-based
  "evaluate item based recommender"
  [recommender sim-fun]
  (let [data-model (.getDataModel recommender)
        evaluator (AverageAbsoluteDifferenceRecommenderEvaluator.)
        builder   (item-based-recommend-builder sim-fun)]
    (.evaluate evaluator builder, nil, data-model, 0.9, 1.0)))

(defn evaluate-user-based-threshold
  "evaluate a user based recommender with threshold neighborhood"
  [recommender sim-fun threshold]
  (let [data-model (.getDataModel recommender)
        evaluator (AverageAbsoluteDifferenceRecommenderEvaluator.)
        builder   (user-based-recommend-threshold-builder sim-fun threshold)]
    (.evaluate evaluator builder, nil, data-model, 0.9, 1.0)))

(defn evaluate-user-based-nearest
  "evaluate a user based recommender with nearest neighborhood"
  [recommender sim-fun nb]
  (let [data-model (.getDataModel recommender)
        evaluator (AverageAbsoluteDifferenceRecommenderEvaluator.)
        builder   (user-based-recommend-nearest-builder sim-fun nb)]
    (.evaluate evaluator builder, nil, data-model, 0.9, 1.0)))

(defn evaluate-all
  "given a file with data model, print all evaluations"
  [filepath]
  (let [sim-funs [user-sim-pearson
                  user-sim-cityblock
                  user-sim-cosine
                  user-sim-tanimoto
                  user-sim-loglikelihood
                  user-sim-euclidean
                  ]]
    (doseq [sim-fun sim-funs]
      (let [reco-user-threshold (file->user-based-threshold-recommender filepath sim-fun 0.1)
            reco-user-nearest (file->user-based-nearestN-recommender filepath sim-fun 20)
            reco-item (file->item-based-recommender filepath sim-fun)]
        (println "eval user based threshold " sim-fun)
        (let [evaluations (repeatedly 10 #(evaluate-user-based-threshold reco-user-threshold sim-fun 0.1))]
          (println evaluations)
          (println "avg: " (/ (apply + evaluations) (count evaluations)))
          )
        (println "eval user based nearest " sim-fun)
        (let [evaluations (repeatedly 10 #(evaluate-user-based-nearest reco-user-nearest sim-fun 3))]
          (println evaluations)
          (println "avg: " (/ (apply + evaluations) (count evaluations)))
          )
        (println "eval item  based " sim-fun)
        (let [evaluations (repeatedly 10 #(evaluate-item-based reco-user-nearest sim-fun))]
          (println evaluations)
          (println "avg: " (/ (apply + evaluations) (count evaluations)))
          )
        ))
    ))


;; prepare data
(defn ids->long-ids-map
  [ids]
  (->> (map-indexed #(hash-map %1 %2) ids)
       (reduce into {})))

(defn- update-ids
  [inv-ids prefs]
  (->> (map (fn [[id p]] {(get inv-ids id) p}) prefs)
       (reduce into {})))

(defn prefs->long-prefs
  [inv-ids-map prefs]
  (->> (fmap (partial update-ids inv-ids-map) prefs)
       (update-ids inv-ids-map))
  )

(defn  flatten-team-pref
  [team-id prefs]
  (map (fn [[opp-id score]]
         [team-id opp-id (double score)])
       prefs))

(defn flatten-prefs
  [prefs]
  (mapcat (fn [[id p]] (flatten-team-pref id p))
          prefs))

(defn export->prefs
  "export preferences to Mahout format, one file with long ids for Mahout and one with ids mapping for application"
  [prefs all-ids filename]
  (with-open [prefs-wtr (io/writer (str filename ".csv"))
              ids-wtr   (io/writer (str filename "-ids.csv"))]
    (let [ids-map     (ids->long-ids-map all-ids)
          inv-ids-map (map-invert ids-map)
          long-prefs  (prefs->long-prefs inv-ids-map prefs)
          ]
      (csv/write-csv prefs-wtr (flatten-prefs long-prefs))
      (csv/write-csv ids-wtr ids-map))
    ))

